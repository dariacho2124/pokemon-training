import { useEffect, useState } from "react";

import { PokemonContext } from "./components/PokemonContext";
import PokemonList from "./components/list/PokemonList";
import PokemonSearch from "./components/search/PokemonSearch";
import PokemonAdd from "./components/add/PokemonAdd";
import ReactJsAlert from "reactjs-alert";
function App() {
  const [data, setData] = useState();
  const [status, setStatus] = useState(false);
  const [type, setType] = useState("");
  const [title, setTitle] = useState("");

  useEffect(() => {
    if (data?.notify) {
      const { msg, action } = data.notify;
      notify(msg, action);
    }
  }, [data?.notify]);
  const notify = (msg, action) => {
    setStatus(true);
    setType(action);
    setTitle(msg);
  };
  return (
    <PokemonContext.Provider value={{ data, setData }}>
      <h2 className="margin-0"> Listado de Pokemons</h2>

      <PokemonSearch />

      <PokemonList />

      {(data?.id || data?.new) && <PokemonAdd />}
      <ReactJsAlert
        status={status}
        type={type}
        title={title}
        quotes={true}
        quote="Gracias"
        Close={() => setStatus(false)}
        button="Confirmar"
      />
    </PokemonContext.Provider>
  );
}

export default App;
