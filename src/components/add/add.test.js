import React from "react";
import "@testing-library/jest-dom/extend-expect";
import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";
import PokemonAdd from "./PokemonAdd";
import { PokemonContext } from "../PokemonContext";
import PokemonService from "../../services/PokemonService";
import pokemonListMock from "../../services/mockDataFunction/mock.data";
import helper from "../../helpers/helper";
describe("Testing initial <PokemonAdd />", () => {
  const data = {
    data: {},
    setData: jest.fn(),
  };
  let pokemonAdd;
  const pokemonFake = {
    id: 7062,
    name: "Snorlax",
    image: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/143.png",
    type: "normal",
    hp: 100,
    attack: 41,
    defense: 100,
    idAuthor: 1,
    created_at: "2022-05-12T18:36:33.687Z",
    updated_at: "2022-05-14T06:25:23.052Z",
  };
  beforeEach(() => {
    pokemonAdd = render(
      <PokemonContext.Provider value={{ ...data }}>
        <PokemonAdd />
      </PokemonContext.Provider>,
    );
  });

  test("Cancel Pokemon", () => {
    const mockHandler = jest.fn();

    const cancel = pokemonAdd.getByText("Cancelar");
    fireEvent.click(cancel);
    expect(cancel).toBeEnabled();
    expect(mockHandler).toHaveBeenCalledTimes(0);
  });
  test("Change name and image", () => {
    const mockHandler = jest.fn();

    const name = pokemonAdd.container.querySelector("#name");
    const image = pokemonAdd.container.querySelector("#image");
    fireEvent.change(name);
    fireEvent.change(image);

    expect(mockHandler).toBeTruthy();
  });
  test("Create Pokemon", async () => {
    const pokemon = {
      attack: 99,
      defense: 70,
      hp: 100,
      type: "normal",
      image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
      name: "Charmander-jg v2",
      idAuthor: 1,
    };
    jest.spyOn(PokemonService, "create").mockImplementation((pokemon) => {
      return pokemonListMock.push(pokemon) ? { status: 200 } : { status: 500 };
    });
    const create = await PokemonService.create(pokemon);
    expect(create.status).toEqual(200);
  });
  test("Create Pokemon fail", async () => {
    const pokemon = {
      attack: 99,
      defense: 70,
      type: "normal",
      image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
      name: "Charmander-jg v2",
      idAuthor: 1,
    };

    jest.spyOn(PokemonService, "create").mockImplementation((pokemon) => {
      const validate = Object.keys(helper.pokemon).map((e) => {
        if (pokemon[e]) {
          return true;
        }
      });
      return validate ? { status: 500 } : { status: 200 };
    });
    const create = await PokemonService.create(pokemon);
    expect(create.status).toEqual(500);
  });
  test("Update Pokemon", async () => {
    const pokemon = {
      attack: 91,
      defense: 68,
      hp: 100,
      type: "normal",
      image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
      name: "Charmander-jg v2",
      idAuthor: 1,
    };
    jest.spyOn(PokemonService, "update").mockImplementation((id, pokemon) => {
      return pokemonListMock.find((p) => p.id == id) ? { status: 200 } : { status: 500 };
    });
    const update = await PokemonService.update(7062, pokemon);
    expect(update.status).toEqual(200);
  });
  test("Update Pokemon fail", async () => {
    const pokemon = {
      attack: 91,
      defense: 68,
      hp: 100,
      type: "normal",
      image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
      name: "Charmander-jg v2",
      idAuthor: 1,
    };
    jest.spyOn(PokemonService, "update").mockImplementation((id, pokemon) => {
      return pokemonListMock.find((p) => p.id == id) ? { status: 200 } : { status: 500 };
    });
    const update = await PokemonService.update(70623, pokemon);
    expect(update.status).toEqual(500);
  });
  test("Get Pokemon id", async () => {
    jest.spyOn(PokemonService, "get").mockImplementation((id, pokemon) => {
      return pokemonListMock.find((p) => p.id == id);
    });
    const getPokemon = await PokemonService.get(7062);

    expect(getPokemon).toEqual(pokemonFake);
  });
  test("Save Pokemon", () => {
    const mockHandler = jest.fn();
    const save = pokemonAdd.getByText("Guardar");

    fireEvent.click(save);
    expect(save).toBeDisabled();
    expect(mockHandler).toHaveBeenCalledTimes(0);
  });
});
