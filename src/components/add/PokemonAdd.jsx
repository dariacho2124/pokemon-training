import React, { useContext, useEffect, useState } from "react";
import PokemonService from "../../services/PokemonService";
import { PokemonContext } from "../PokemonContext";
import helper from "../../helpers/helper";
import "./styles.css";

const PokemonAdd = () => {
  const [pokemon, setPokemon] = useState(helper.pokemon);
  const { data, setData } = useContext(PokemonContext);
  useEffect(() => {
    const getPokemon = () => {
      PokemonService.get(data?.id)
        .then((response) => {
          setPokemon({ ...response?.data });
        })
        .catch((e) => {});
    };
    if (data?.id) getPokemon();
  }, [data?.id]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setPokemon({ ...pokemon, [name]: value });
  };

  const savePokemon = () => {
    data?.id ? update() : create();
  };

  const create = () => {
    PokemonService.create(pokemon)
      .then((response) => {
        setData({ update: true, notify: { msg: "Pokemon creado", action: "success" } });
      })
      .catch((e) => {
        setData({ notify: { msg: "Pokemon no creado", action: "error" } });
      });
  };
  const update = () => {
    PokemonService.update(data?.id, pokemon)
      .then((response) => {
        setData({ update: true, notify: { msg: "Pokemon actulizado", action: "info" } });
      })
      .catch((e) => {
        setData({ notify: { msg: "Pokemon no actulizado", action: "error" } });
      });
  };

  const closeAddComponet = () => {
    setData({});
  };

  return (
    <div className="panel">
      <div className="centerText">
        <span>Nuevo Pokemón</span>
      </div>

      <ul>
        <li>
          <div className="inputText">
            <div className="nameInput">
              <label htmlFor="name">Nombre: </label>
              <input
                id="name"
                name="name"
                value={pokemon.name}
                onChange={handleInputChange}
                className={pokemon.name ? "inputForm inputValid" : "inputForm  inputInvalid"}
                placeholder="Nombre"
                type="text"
              />
            </div>
            <br />
            <div className="imgInput">
              <label htmlFor="image">Imagen: </label>
              <input
                id="image"
                name="image"
                value={pokemon.image}
                onChange={handleInputChange}
                className={pokemon.image ? "inputForm inputValid" : "inputForm  inputInvalid"}
                type="text"
                placeholder="Url de imagen"
              />
              <br />

              {!helper.urlValid.test(pokemon.image) && pokemon.image && (
                <small className="errorMessage">Error de Url</small>
              )}
            </div>
          </div>
        </li>
        <li className="rangePanelBtn">
          <div className="rangeBtnAttack">
            <label htmlFor="attack" className="la">
              Ataque: {pokemon.attack}{" "}
            </label>
            <input
              id="attack"
              name="attack"
              value={pokemon.attack}
              onChange={handleInputChange}
              type="range"
              min="0"
              max={pokemon.hp}
            />
            <label htmlFor=""> {pokemon.hp}</label>
          </div>
          <br />
          <div className="rangeBtn">
            <label htmlFor="defense" className="la">
              Defensa: {pokemon.defense}{" "}
            </label>
            <input
              id="defense"
              name="defense"
              value={pokemon.defense}
              onChange={handleInputChange}
              type="range"
              min="0"
              max={pokemon.hp}
            />
            <label> {pokemon.hp}</label>
          </div>
        </li>
      </ul>

      <div className="action">
        <br />
        <button
          className={
            !helper.verifyPokemonInfo(pokemon) ? "button buttonInactive pointer" : "button buttonActive pointer"
          }
          disabled={!helper.verifyPokemonInfo(pokemon)}
          onClick={savePokemon}
        >
          <span className="material-symbols-outlined">save</span>Guardar
        </button>
        <button onClick={() => closeAddComponet()} alt="cancel" className="button buttonActive pointer">
          <span className="material-symbols-outlined">close</span>
          Cancelar
        </button>
      </div>
    </div>
  );
};
export default PokemonAdd;
