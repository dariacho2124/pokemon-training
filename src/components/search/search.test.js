import React from "react";
import "@testing-library/jest-dom/extend-expect";
import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";
import PokemonSearch from "./PokemonSearch";
import { PokemonContext } from "../PokemonContext";
describe("Testing initial <PokemonList />", () => {
  const data = {
    data: {},
    setData: jest.fn(),
  };
  let pokemonSearch;
  beforeEach(() => {
    pokemonSearch = render(
      <PokemonContext.Provider value={{ ...data }}>
        <PokemonSearch />
      </PokemonContext.Provider>,
    );
  });
  test("Change Search pokemon", () => {
    const mockHandler = jest.fn();
    fireEvent.change(pokemonSearch.container.querySelector("#search"));

    expect(mockHandler).toBeTruthy();
  });

  test("Pokemon new pokemon", () => {
    const mockHandler = jest.fn();
    fireEvent.click(pokemonSearch.container.querySelector("#new"));
    expect(mockHandler).toHaveBeenCalledTimes(0);
  });
});
