import React, { useContext, useState } from "react";
import { PokemonContext } from "../PokemonContext";
import "./styles.css";
const PokemonSearch = () => {
  const [search, setSearch] = useState();
  const { setData } = useContext(PokemonContext);

  const handleInputChange = (event) => {
    const { value } = event.target;
    setSearch(value);
    setData({ search: value });
  };
  return (
    <div className="searchPanel">
      <input type="text" id="search" onChange={handleInputChange} name="search" className="search" />
      {!search && (
        <>
          <span className="searchIcon material-symbols-outlined">search</span>
          <span className="searchText">Buscar</span>
        </>
      )}
      <button className="new pointer" id="new" onClick={() => setData({ id: null, new: true })}>
        <span className="material-symbols-outlined">add</span>
        Nuevo
      </button>
    </div>
  );
};
export default PokemonSearch;
