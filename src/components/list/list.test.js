import React from "react";
import "@testing-library/jest-dom/extend-expect";
import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import PokemonList from "./PokemonList";
import { PokemonContext } from "../PokemonContext";
import PokemonService from "../../services/PokemonService";
import pokemonListMock from "../../services/mockDataFunction/mock.data";
describe("Testing initial <PokemonList />", () => {
  const data = {
    data: {},
    setData: jest.fn(),
  };
  let pokemonList;
  beforeEach(() => {
    pokemonList = render(
      <PokemonContext.Provider value={{ ...data }}>
        <PokemonList />
      </PokemonContext.Provider>,
    );
  });
  test("Pokemon edit click", () => {
    const mockHandler = jest.fn();
    pokemonList.container.querySelector("#edit");

    expect(mockHandler).toHaveBeenCalledTimes(0);
  });

  test("Pokemon delete click", () => {
    const mockHandler = jest.fn();
    pokemonList.container.querySelector("#del");

    expect(mockHandler).toHaveBeenCalledTimes(0);
  });
  test("Get Pokemons", async () => {
    jest.spyOn(PokemonService, "getAll").mockImplementation(() => {
      return pokemonListMock;
    });
    const pokemons = await PokemonService.getAll();

    expect(pokemons.length).toEqual(5);
  });
});
