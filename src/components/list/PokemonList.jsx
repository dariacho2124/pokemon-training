import React, { useState, useEffect, useContext } from "react";
import PokemonService from "../../services/PokemonService";
import { PokemonContext } from "../PokemonContext";

import "./styles.css";
const PokemonList = () => {
  const [pokemon, setPokemons] = useState([]);
  const { data, setData } = useContext(PokemonContext);

  useEffect(() => {
    listPokemon();
  }, [data?.update]);

  const listPokemon = () => {
    PokemonService.getAll()
      .then((response) => {
        setPokemons(response.data);
      })
      .catch((e) => {});
  };
  const searchData = (list, filter) => {
    let result = [];
    filter
      ? list.forEach((e) => {
          if (e.name.toLowerCase().indexOf(data?.search.toLowerCase()) !== -1) result.push(e);
        })
      : (result = list);
    return result;
  };
  const editPokemon = (id) => {
    setData({ id });
  };

  const deletePokemon = (id) => {
    setData({ update: false });
    PokemonService.remove(id)
      .then((response) => {
        setData({ update: true, notify: { msg: "Pokemon eliminado", action: "success" } });
      })
      .catch((e) => {
        setData({ notify: { msg: "Pokemon no eliminado", action: "error" } });
      });
  };

  return (
    <div className={data?.id || data?.new ? "tableClass" : "tableClassTotal"}>
      <table>
        <thead>
          <tr className="headerTable">
            <td>
              <strong>Nombre</strong>{" "}
            </td>
            <td>
              <strong>Imagen</strong>{" "}
            </td>
            <td>
              <strong>Ataque</strong>{" "}
            </td>
            <td>
              <strong>Defensa</strong>{" "}
            </td>
            <td className="paddingAction">
              <strong>Acciones</strong>{" "}
            </td>
          </tr>
        </thead>
        <tbody className={(data?.id || data?.new) && "tbodyClass"}>
          {searchData(pokemon, data?.search).map((item, index) => (
            <tr key={index}>
              <td>{item.name}</td>
              <td>
                <img src={item.image} className="imgPreview" alt="some value" />
              </td>
              <td>{item.attack}</td>
              <td>{item.defense}</td>
              <td className="actionList">
                <span onClick={() => editPokemon(item.id)} id="edit" className="material-symbols-outlined edit pointer">
                  border_color
                </span>
                <span onClick={() => deletePokemon(item.id)} id="del" className="material-symbols-outlined pointer">
                  delete_forever
                </span>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default PokemonList;
