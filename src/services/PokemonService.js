import http from "./HttpService";

const getAll = () => {
  return http.get("?idAuthor=1");
};

const get = (id) => {
  return http.get(`${id}`);
};

const create = (data) => {
  return http.post("?idAuthor=1", data);
};

const update = (id, data) => {
  return http.put(`${id}`, data);
};

const remove = (id) => {
  return http.delete(`${id}`);
};

const PokemonService = {
  getAll,
  get,
  create,
  update,
  remove,
};

export default PokemonService;
