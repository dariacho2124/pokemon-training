const pokemonListMock = [
  {
    id: 7062,
    name: "Snorlax",
    image: "https://assets.pokemon.com/assets/cms2/img/pokedex/full/143.png",
    type: "normal",
    hp: 100,
    attack: 41,
    defense: 100,
    idAuthor: 1,
    created_at: "2022-05-12T18:36:33.687Z",
    updated_at: "2022-05-14T06:25:23.052Z",
  },
  {
    id: 7084,
    name: "Charmanders",
    image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
    type: "normal",
    hp: 100,
    attack: 87,
    defense: 69,
    idAuthor: 1,
    created_at: "2022-05-13T18:06:43.584Z",
    updated_at: "2022-05-14T17:05:51.018Z",
  },
  {
    id: 7086,
    name: "Charmander-jg",
    image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
    type: "normal",
    hp: 100,
    attack: 90,
    defense: 69,
    idAuthor: 1,
    created_at: "2022-05-13T18:10:04.972Z",
    updated_at: "2022-05-13T18:10:04.972Z",
  },
  {
    id: 7119,
    name: "Charmander-jg",
    image: "https://images.wikidexcdn.net/mwuploads/wikidex/5/56/latest/20200307023245/Charmander.png",
    type: "normal",
    hp: 100,
    attack: 91,
    defense: 68,
    idAuthor: 1,
    created_at: "2022-05-14T13:48:55.774Z",
    updated_at: "2022-05-14T13:48:55.774Z",
  },
  {
    id: 7144,
    name: "viejo actulizado",
    image: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTwf8hpOyntUXOVjMNreQsK5BomnU47ap4p5A&usqp=CAU",
    type: "normal",
    hp: 100,
    attack: 1,
    defense: 1,
    idAuthor: 1,
    created_at: "2022-05-14T18:48:18.958Z",
    updated_at: "2022-05-14T18:48:18.958Z",
  },
];
export default pokemonListMock;
