import axios from "axios";

export default axios.create({
  baseURL: "https://pokemon-pichincha.herokuapp.com/pokemons/",
  headers: {
    "Content-type": "application/json",
  },
});
