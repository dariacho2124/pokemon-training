const urlValid =
  /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/; // eslint-disable-line
const verifyPokemonInfo = (data) => {
  let active = true;
  Object.keys(data).map((element) => {
    // eslint-disable-line
    if (!data[element]) return (active = false);
  });

  return !urlValid.test(data.image) ? false : active;
};
const pokemon = {
  name: "",
  hp: 100,
  idAuthor: 1,
  type: "normal",
  image: "",
  defense: 1,
  attack: 1,
};
const helper = {
  urlValid,
  verifyPokemonInfo,
  pokemon,
};

export default helper;
